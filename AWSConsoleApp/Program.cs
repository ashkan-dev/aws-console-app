﻿using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.Runtime;
using Amazon.Runtime.CredentialManagement;
using AWSConsoleApp.Ops;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Threading.Tasks;

namespace AWSConsoleApp
{
    class Program
    {
        private static ImmutableCredentials credentials;

        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Welcome to AWS");

                AWSCredentials iam = ConnectToAWS();
                credentials = iam.GetCredentials();
                Console.WriteLine("Connected to IAM Successfully");
                MainMenu();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
        }

        private static AWSCredentials ConnectToAWS()
        {
            Console.Write("Enter your access key: ");
            string accessKey = Console.ReadLine();

            Console.WriteLine();
            Console.Write("Enter your secret key: ");
            string secretKey = Console.ReadLine();

            var options = new CredentialProfileOptions
            {
                AccessKey = accessKey,
                SecretKey = secretKey
            };
            var profile = new CredentialProfile("basic_profile", options);
            profile.Region = RegionEndpoint.APSoutheast1;

            var netSDKFile = new NetSDKCredentialsFile();
            netSDKFile.RegisterProfile(profile);

            return profile.GetAWSCredentials(netSDKFile);
        }

        private static void MainMenu()
        {
            Console.Write("Select next op Bucket (b) / DynamoDB (d) / SQS (S) / System Manager (m) / Exit (e): ");
            string op = Console.ReadLine();

            switch (op)
            {
                case "b":
                case "B":
                    new BucketOp(credentials);
                    NextOp();
                    break;
                case "d":
                case "D":
                    new DynamoDbOps(credentials);
                    NextOp();
                    break;
                case "s":
                case "S":
                    new SqsOps();
                    NextOp();
                    break;
                case "m":
                case "M":
                    new SysManagerOps(credentials);
                    NextOp();
                    break;
                case "e":
                case "E":
                    Console.Write("Press any key to exit");
                    break;
            }
        }

        private static void NextOp()
        {
            Console.ReadKey();
            MainMenu();
        }
    }
}
