﻿using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using Amazon.Runtime;
using Entities;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace AWSConsoleApp
{
    public class DynamoDbOps
    {
        private ImmutableCredentials credentials;

        public DynamoDbOps(ImmutableCredentials credentials)
        {
            this.credentials = credentials;
            ops();
        }

        private void ops()
        {
            IAmazonDynamoDB client = new AmazonDynamoDBClient(credentials.AccessKey, credentials.SecretKey, RegionEndpoint.APSoutheast1);
            DynamoDBContext context = new DynamoDBContext(client);
            Console.WriteLine("List of Tables:");
            ListTablesResponse tables = client.ListTablesAsync().Result;
            foreach (string name in tables.TableNames)
            {
                Console.WriteLine(name);
            }

            Console.Write("Enter table name to connect: ");
            string tblName = Console.ReadLine();
            Table tbl = Table.LoadTable(client, tblName);

            Console.Write("What do you wanna do (Insert (i) / Read data (r) / Query (q) / Upd (u)? ");
            string act = Console.ReadLine();

            switch (act)
            {
                case "i":
                case "I":
                    InsertToDynamoDB(tblName, context);
                    break;
                case "r":
                case "R":
                    ReadData(tbl, context);
                    break;
                case "Q":
                case "q":
                    QueryDynamoDbData(tbl, client);
                    break;
                case "U":
                case "u":
                    UpdDynamoDBTbl(tbl.TableName, client);
                    break;
            }
        }

        private void QueryDynamoDbData(Table tbl, IAmazonDynamoDB client)
        {
            Console.Write("Query with Artist key (a) / studio key (s) / List of Indexes (i): ");
            string key = Console.ReadLine();
            List<Music> musics = new List<Music>();

            List<PropertyInfo> props = typeof(Songs).GetProperties().ToList();
            var propNames = new List<string>();
            props.ForEach(prop => propNames.Add(prop.Name));

            switch (key)
            {
                case "a":
                case "A":
                    Console.Write("Enter desired index name: ");
                    string aidx = Console.ReadLine();

                    var request = new QueryRequest
                    {
                        TableName = tbl.TableName,
                        IndexName = aidx,
                        Select = "ALL_ATTRIBUTES",
                        ReturnConsumedCapacity = "TOTAL",
                        ExpressionAttributeValues = new Dictionary<string, AttributeValue>
                        {
                            {
                                ":v_Artist", new AttributeValue
                                { S =  "Aubrey Bignold" }
                            },
                            {
                                ":v_price", new AttributeValue
                                { S =  "37" }
                            }
                        },
                        KeyConditionExpression = "Artist = :v_Artist AND Price > :v_price"
                    };

                    var items = client.QueryAsync(request).Result.Items;

                    items.ForEach(item => musics.Add(new Music
                    {
                        Artist = item[propNames[0]].S,
                        SongTitle = item[propNames[1]].S,
                        Price = item[propNames[2]].S,
                        address = item[propNames[3]].S,
                        studio = item[propNames[4]].S
                    }));
                    DisplayMusicResult(musics, tbl.TableName);
                    break;
                case "s":
                case "S":
                    var req = new QueryRequest
                    {
                        TableName = tbl.TableName,
                        IndexName = tbl.GlobalSecondaryIndexNames[1],
                        Select = "ALL_ATTRIBUTES",
                        ReturnConsumedCapacity = "TOTAL",
                        ExpressionAttributeValues = new Dictionary<string, AttributeValue>
                        {
                            {
                                ":v_studio", new AttributeValue
                                { S =  "Trinseo S.A." }
                            },
                            {
                                ":v_price", new AttributeValue
                                { S =  "37" }
                            }
                        },
                        KeyConditionExpression = "studio = :v_studio AND Price > :v_price"
                    };

                    var musicItems = client.QueryAsync(req).Result.Items;

                    musicItems.ForEach(item => musics.Add(new Music
                    {
                        Artist = item[propNames[0]].S,
                        SongTitle = item[propNames[1]].S,
                        Price = item[propNames[2]].S,
                        address = item[propNames[3]].S,
                        studio = item[propNames[4]].S
                    }));
                    DisplayMusicResult(musics, tbl.TableName);
                    break;
                case "i":
                case "I":
                    Console.WriteLine("Local Secondary Idxes:");
                    tbl.LocalSecondaryIndexNames.ForEach(name => Console.WriteLine(name));

                    Console.WriteLine("Global Secondary Idxes:");
                    tbl.GlobalSecondaryIndexNames.ForEach(name => Console.WriteLine(name));
                    break;
            }
        }

        private void InsertToDynamoDB(string tblName, DynamoDBContext context)
        {
            Console.Write("Enter json file path: ");
            string path = Console.ReadLine();

            using (StreamReader r = new StreamReader(path))
            {
                string json = r.ReadToEnd();

                if (tblName == typeof(Songs).GetAttributeValue((DynamoDBTableAttribute dna) => dna.TableName))
                {
                    List<Songs> items = JsonConvert.DeserializeObject<List<Songs>>(json);
                    items.ForEach(item => context.SaveAsync<Songs>(item));
                }
                else if (tblName == typeof(Music).GetAttributeValue((DynamoDBTableAttribute dna) => dna.TableName))
                {
                    List<Music> items = JsonConvert.DeserializeObject<List<Music>>(json);
                    items.ForEach(item => context.SaveAsync<Music>(item));
                }
                else if (tblName == typeof(MusicThree).Name)
                {
                    List<MusicThree> items = JsonConvert.DeserializeObject<List<MusicThree>>(json);
                    items.ForEach(item => context.SaveAsync<MusicThree>(item));
                }
                else if (tblName == typeof(Demo).GetAttributeValue((DynamoDBTableAttribute dna) => dna.TableName))
                {
                    List<Demo> items = JsonConvert.DeserializeObject<List<Demo>>(json);
                    items.ForEach(item => context.SaveAsync<Demo>(item));
                }

                Console.WriteLine("Data insertion is completed.");
            }
        }

        private void UpdDynamoDBTbl(string tblName, IAmazonDynamoDB client)
        {
            Console.Write("Usual Upd (u) / Conditional Upd (c): ");
            string upd = Console.ReadLine();

            if (upd == "u" || upd == "U")
            {
                var request = new UpdateItemRequest
                {
                    TableName = tblName,
                    Key = new Dictionary<string, AttributeValue>
                    {
                        { "Artist", new AttributeValue { S = "Cordie Ackery" } },
                        { "SongTitle", new AttributeValue { S = "Go Figure (Va savoir)" } }
                    },
                    ExpressionAttributeValues = new Dictionary<string, AttributeValue>
                    {
                        {":p",new AttributeValue {N = "1"}}
                    },
                    UpdateExpression = "SET Price = Price + :p",
                    ReturnValues = "UPDATED_NEW"
                };
                UpdResult(request, client);
            }
            else if (upd == "c" || upd == "C")
            {
                Console.Write("Enter current val: ");
                string val = Console.ReadLine();

                Console.WriteLine();

                Console.Write("Enter new val: ");
                string newVal = Console.ReadLine();

                Console.WriteLine();

                var request = new UpdateItemRequest
                {
                    TableName = tblName,
                    Key = new Dictionary<string, AttributeValue>
                    {
                        { "Artist", new AttributeValue { S = "Cordie Ackery" } },
                        { "SongTitle", new AttributeValue { S = "Go Figure (Va savoir)" } }
                    },
                    ExpressionAttributeValues = new Dictionary<string, AttributeValue>
                    {
                        {":p",new AttributeValue {N = newVal}},
                        {":currp",new AttributeValue {N = val}}
                    },
                    UpdateExpression = "SET Price = :p",
                    ConditionExpression = "Price = :currp",
                    ReturnValues = "ALL_NEW"
                };
                UpdResult(request, client);
            }
        }

        private void UpdResult(UpdateItemRequest req, IAmazonDynamoDB client)
        {
            var response = client.UpdateItemAsync(req).Result;
            Console.WriteLine("Upd finalized");
        }

        private void ReadData(Table tbl, DynamoDBContext context)
        {
            if (tbl.TableName == typeof(Songs).GetAttributeValue((DynamoDBTableAttribute dna) => dna.TableName))
            {
                List<Songs> response = context.ScanAsync<Songs>(null).GetNextSetAsync().Result;
                DisplayMusicResult(response, null);
            }
            else if (tbl.TableName == typeof(Music).GetAttributeValue((DynamoDBTableAttribute dna) => dna.TableName))
            {
                List<Music> response = context.ScanAsync<Music>(null).GetNextSetAsync().Result;
                DisplayMusicResult(response, tbl.TableName);
            }
            else if (tbl.TableName == typeof(MusicThree).Name)
            {
                List<MusicThree> response = context.ScanAsync<MusicThree>(null).GetNextSetAsync().Result;
                DisplayMusicResult(response, tbl.TableName);
            }
        }

        private void DisplayMusicResult(IList list, string tableName)
        {
            if (tableName == typeof(Songs).GetAttributeValue((DynamoDBTableAttribute dna) => dna.TableName))
            {
                List<PropertyInfo> props = typeof(Songs).GetProperties().ToList();
                CreateHeaders(props);

                ((List<Songs>)list).ForEach(song =>
               {
                   Console.Write("\t" + song.Artist);
                   Console.Write("\t" + song.SongTitle);
                   Console.Write("\t" + song.Price);
                   Console.Write("\t" + song.address);
                   Console.Write("\t" + song.studio);
                   Console.WriteLine();
               });
            }
            else if (tableName == typeof(Music).GetAttributeValue((DynamoDBTableAttribute dna) => dna.TableName))
            {
                List<PropertyInfo> props = typeof(Music).GetProperties().ToList();
                CreateHeaders(props);

                ((List<Music>)list).ForEach(music =>
               {
                   Console.Write("\t" + music.Artist);
                   Console.Write("\t" + music.SongTitle);
                   Console.Write("\t" + music.Price);
                   Console.Write("\t" + music.address);
                   Console.Write("\t" + music.studio);
                   Console.WriteLine();
               });
            }
            else
            {
                Console.WriteLine("There is no data");
            }
        }

        private void CreateHeaders(List<PropertyInfo> props)
        {
            var propNames = new List<string>();
            props.ForEach(prop => Console.Write("\t\t" + prop.Name));
            Console.WriteLine();
        }
    }
}
