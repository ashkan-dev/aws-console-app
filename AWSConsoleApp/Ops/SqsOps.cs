﻿using Amazon.SQS;
using Amazon.SQS.Model;
using System;
using System.Collections.Generic;
using System.Net;

namespace AWSConsoleApp
{
    public class SqsOps
    {
        private List<string> urls { get; set; }

        public SqsOps()
        {
            urls = new List<string>();
            GetUrls();
        }

        private void GetUrls()
        {
            var client = new AmazonSQSClient();
            var req = new ListQueuesRequest();
            ListQueuesResponse response = client.ListQueuesAsync(req).Result;
            urls.AddRange(response.QueueUrls);

            byte counter = 1;
            Console.WriteLine("Queues URLs");
            urls.ForEach((string url) =>
            {
                Console.WriteLine(string.Format("{0}. {1}", counter++, url));
            });

            Console.Write("Send msg (S) / Delete (D): ");
            string read = Console.ReadLine();

            switch (read)
            {
                case "s":
                case "S":
                    SendMsg(client);
                    break;
                case "d":
                case "D":
                    ClearQ();
                    break;
            }
        }

        private void SendMsg(AmazonSQSClient client)
        {
            Console.Write("Select nember of queue URL: ");
            string num = Console.ReadLine();
            int idx = Convert.ToByte(num) - 1;

            if (idx >= urls.Count)
            {
                Console.WriteLine("Enter in range num.");
                SendMsg(client);
                return;
            }

            Console.Write("Enter msg you wanna send: ");
            string msg = Console.ReadLine();

            var req = new SendMessageRequest
            {
                MessageBody = msg,
                QueueUrl = urls[idx]
            };
            SendMessageResponse response = client.SendMessageAsync(req).Result;

            Console.WriteLine("SQS msg has been sent successfully.");
            Console.WriteLine(string.Format("Msg Id: {0}", response.MessageId));
        }

        private void ClearQ()
        {
            var client = new AmazonSQSClient();

            Console.Write("Select queue number in the list: ");
            string num = Console.ReadLine();

            var req = new ReceiveMessageRequest
            {
                QueueUrl = urls[Convert.ToInt32(num) - 1],
                AttributeNames = new List<string> { "SentTimestamp" },
                MaxNumberOfMessages = 10,
                WaitTimeSeconds = 15
            };

            byte count = 1;
            ReceiveMessageResponse response = client.ReceiveMessageAsync(req).Result;

            response.Messages.ForEach((Message msg) =>
            {
                Console.WriteLine(string.Format("{0}. Msg Id: {1}", count++, msg.MessageId));
                Console.WriteLine(string.Format("Msg body: {0}", msg.Body));
                var date = new DateTime(1970, 1, 1).AddMilliseconds(double.Parse(msg.Attributes["SentTimestamp"]));
                Console.WriteLine(string.Format("Sent date: {0}", date));
                Console.WriteLine();
            });

            PurgeQ((Convert.ToInt32(num) - 1), response.Messages);
        }

        private void PurgeQ(int num, List<Message> msgs)
        {
            Console.Write("Enter 'all' to purge all msgs or msg number to delete msg: ");
            string inp = Console.ReadLine();

            var client = new AmazonSQSClient();

            switch (inp)
            {
                case "all":
                case "All":
                    var purgeReq = new PurgeQueueRequest
                    {
                        QueueUrl = urls[num]
                    };
                    PurgeQueueResponse purgeResponse = client.PurgeQueueAsync(purgeReq).Result;

                    if (purgeResponse.HttpStatusCode == HttpStatusCode.OK)
                    {
                        Console.WriteLine("Queue has been purged successfully.");
                    }
                    else
                    {
                        Console.WriteLine("Queue not deleted. Error happened.");
                    }

                    break;
                default:
                    var delReq = new DeleteMessageRequest
                    {
                        QueueUrl = urls[num],
                        ReceiptHandle = msgs[Convert.ToInt32(inp) - 1].ReceiptHandle
                    };

                    DeleteMessageResponse delResponse = client.DeleteMessageAsync(delReq).Result;
                    if (delResponse.HttpStatusCode == HttpStatusCode.OK)
                    {
                        Console.WriteLine("Msg has been deleted successfully.");
                    }
                    else
                    {
                        Console.WriteLine("Msg not deleted. Error happened.");
                    }
                    break;
            }
        }
    }
}
