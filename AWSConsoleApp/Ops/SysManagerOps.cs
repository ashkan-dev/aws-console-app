﻿using Amazon;
using Amazon.Runtime;
using Amazon.SimpleSystemsManagement;
using Amazon.SimpleSystemsManagement.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace AWSConsoleApp.Ops
{
    public class SysManagerOps
    {
        private readonly ImmutableCredentials credentials;

        public SysManagerOps(ImmutableCredentials credentials)
        {
            this.credentials = credentials;
            SelectOp();
        }

        private void SelectOp()
        {
            Console.Write("Select op(Put param (p) / Get params (g) / Get spec param (s) / Del param (d) / Exit (Enter): ");
            string choice = Console.ReadLine();

            switch (choice)
            {
                case "p":
                case "P":
                    PutParam();
                    break;
                case "g":
                case "G":
                    GetParamsInfo();
                    break;
                case "s":
                case "S":
                    GetSpecParam();
                    break;
                case "d":
                case "D":
                    DelParam();
                    break;
            }
        }

        private void PutParam()
        {
            Console.Write("Do you wanna encrypt data (y / n): ");
            string type = Console.ReadLine();

            Console.Write("Name: ");
            string name = Console.ReadLine();

            Console.Write("Description: ");
            string desc = Console.ReadLine();

            Console.Write("Value: ");
            string val = Console.ReadLine();
            Console.WriteLine();

            var client = new AmazonSimpleSystemsManagementClient(credentials.AccessKey, credentials.SecretKey, RegionEndpoint.APSoutheast1);
            var req = new PutParameterRequest
            {
                Type = type == "y" ? "SecureString" : "String",
                DataType = "text",
                Name = name,
                Description = desc,
                Value = val
            };
            PutParameterResponse response = client.PutParameterAsync(req).Result;
            if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                Console.WriteLine("Parameter successfully inserted");
                Console.WriteLine(string.Format("Tier: {0}", response.Tier.Value));
                Console.WriteLine(string.Format("Version: {0}", response.Version));
            }
            else
            {
                Console.WriteLine("These is a fucking err");
            }
            Console.ReadKey();
            SelectOp();
        }

        private void GetParamsInfo()
        {
            Console.Write("Read encrypted params (y/n)? ");
            string answer = Console.ReadLine();
            var client = new AmazonSimpleSystemsManagementClient(credentials.AccessKey, credentials.SecretKey, RegionEndpoint.APSoutheast1);
            DescribeParametersRequest req = new DescribeParametersRequest
            {
                ParameterFilters = answer == "y" ?
                    new List<ParameterStringFilter>
                    {
                        new ParameterStringFilter
                        {
                            Key = "Type",
                            Values = new List<string> {"SecureString"}
                        }
                    } : new List<ParameterStringFilter>()
            };
            DescribeParametersResponse response = client.DescribeParametersAsync(req).Result;

            if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                Console.WriteLine("Param Stores:");
                response.Parameters.ForEach((ParameterMetadata param) =>
                {
                    Console.WriteLine(string.Format("Name: {0}, Data Type: {1} - Type: {2} - Desc: {3} - Key: {4} - Tier: {5}", param.Name, param.DataType, param.Type, param.Description, param.KeyId, param.Tier));
                });
            }
            else
            {
                Console.WriteLine("there is a fucking err");
            }
            Console.ReadKey();
            SelectOp();
        }

        private void GetSpecParam()
        {
            var client = new AmazonSimpleSystemsManagementClient(credentials.AccessKey, credentials.SecretKey, RegionEndpoint.APSoutheast1);

            Console.Write("Enter param name: ");
            string name = Console.ReadLine();

            Console.Write("Read param with decryption (y/n): ");
            string answer = Console.ReadLine();

            var req = new GetParameterRequest
            {
                Name = name,
                WithDecryption = answer == "y"
            };
            GetParameterResponse response = client.GetParameterAsync(req).Result;

            if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                Console.WriteLine(string.Format("Data Type: {0} - Type: {1} - ARN: {2} - Val: {3}", response.Parameter.DataType, response.Parameter.Type, response.Parameter.ARN, response.Parameter.Value));
            }
            else
            {
                Console.WriteLine("there is a fucking err");
            }
            Console.ReadKey();
            SelectOp();
        }

        private void DelParam()
        {
            var client = new AmazonSimpleSystemsManagementClient(credentials.AccessKey, credentials.SecretKey, RegionEndpoint.APSoutheast1);
            Console.Write("Enter param name to delete: ");
            string name = Console.ReadLine();

            var req = new DeleteParameterRequest { Name = name };
            DeleteParameterResponse response = client.DeleteParameterAsync(req).Result;

            if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                Console.WriteLine("Param deleted successfully");
            }
            else
            {
                Console.WriteLine("deletion unsuccessful because there was a fucking err");
            }
            Console.ReadKey();
            SelectOp();
        }
    }
}
