﻿using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AWSConsoleApp
{
    public class BucketOp
    {
        public ImmutableCredentials credentials;

        public BucketOp(ImmutableCredentials credentials)
        {
            this.credentials = credentials;
            ConnectToBucket();
        }

        private void ConnectToBucket()
        {
            Console.Write("List buckets (B) / List bucket objs (L) / Upload file to bucket (U): ");
            string inp = Console.ReadLine();

            switch (inp)
            {
                case "b":
                case "B":
                    ListBuckets();
                    break;
                case "l":
                case "L":
                    ListBucketObjs();
                    break;
                case "u":
                case "U":
                    UploadFileToBucket();
                    break;
            }
        }

        private void ListBucketObjs()
        {
            Console.Write("Enter bucket name: ");
            string bucket = Console.ReadLine();

            string folderPath = string.Empty;

            if (HasFolder())
            {
                Console.Write("Enter full folders path: ");
                folderPath = Console.ReadLine();
            }

            var request = new ListObjectsRequest
            {
                BucketName = bucket,
                Prefix = folderPath
            };

            IAmazonS3 s3Client = new AmazonS3Client(credentials.AccessKey, credentials.SecretKey, RegionEndpoint.APSoutheast1);
            ListObjectsResponse response = s3Client.ListObjectsAsync(request).Result;

            if (response.S3Objects.Count > 0)
            {
                Console.WriteLine("List of objects of " + bucket);
                response.S3Objects.ForEach(obj => Console.WriteLine(obj.Key));
            }
            else
            {
                Console.WriteLine("No object exists inside of " + bucket);
            }
        }

        private void ListBuckets()
        {
            IAmazonS3 s3Client = new AmazonS3Client(credentials.AccessKey, credentials.SecretKey, RegionEndpoint.APSoutheast1);
            ListBucketsResponse response = s3Client.ListBucketsAsync().Result;

            Console.WriteLine("Buckets list");
            response.Buckets.ForEach(bucket => Console.WriteLine(string.Format("- {0}", bucket.BucketName)));
        }

        private void UploadFileToBucket()
        {
            Console.Write("Enter bucket name: ");
            string bucket = Console.ReadLine();

            IAmazonS3 s3Client = new AmazonS3Client(credentials.AccessKey, credentials.SecretKey, RegionEndpoint.APSoutheast1);
            var fileTransferUtil = new TransferUtility(s3Client);

            Console.Write("Enter your local file path to upload: ");
            string file = Console.ReadLine();
            Console.WriteLine("Uploading...");
            fileTransferUtil.Upload(file, bucket);
            Console.WriteLine("Upload completed successfully");
        }

        private static bool HasFolder()
        {
            Console.Write("Do you wanna enter folder name? (y/n)");
            string response = Console.ReadLine();

            if (response == "y")
            {
                return true;
            }
            else if (response == "n")
            {
                return false;
            }
            else
            {
                Console.WriteLine("Enter y or n");
                return HasFolder();
            }
        }
    }
}
