﻿using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    [DynamoDBTable("Music")]
    public class Songs
    {
        [DynamoDBHashKey]
        public string Artist { get; set; }

        [DynamoDBRangeKey]
        public string SongTitle { get; set; }

        [DynamoDBProperty]
        public string Price { get; set; }

        [DynamoDBProperty]
        public string address { get; set; }

        [DynamoDBProperty]
        public string studio { get; set; }
    }
}
